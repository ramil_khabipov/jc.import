﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace JCImport
{
    public interface IImporter
    {
        Task<int?> RunAsync();
    }
}
