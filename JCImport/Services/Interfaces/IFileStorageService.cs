﻿using System.IO;
using System.Threading.Tasks;

namespace JCImport.Services.Interfaces
{
    public interface IFileStorageService
    {
        Task<string> UploadAsync(string base64, string fileName = "", string contentType = "");

        Task<string> UploadAsync(Stream stream, string fileName = "", string contentType = "");
    }
}
