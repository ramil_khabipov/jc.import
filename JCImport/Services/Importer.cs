﻿using Contentful.Core;
using Contentful.Core.Models;
using Contentful.Core.Models.Management;
using Contentful.Core.Search;
using JCImport.Domain;
using JCImport.Models;
using JCImport.Services.Interfaces;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Reflection;
using System.Threading;
using System.Threading.Tasks;
using File = Contentful.Core.Models.File;

namespace JCImport.Services
{
    public partial class Importer : IImporter
    {
        private readonly ILogger<Importer> logger;
        private readonly IContentfulClient deliveryClient;
        private readonly IContentfulManagementClient client;
        private readonly IFileStorageService fileStorageService;
        private readonly PolopolyDBContext context;
        private readonly HttpClient httpClient;
        private readonly JsonSerializerSettings jsonSettings;

        public Importer(ILogger<Importer> logger,
            IContentfulClient contentfulClient,
            IContentfulManagementClient contentfulManagementClient,
            IFileStorageService fileStorageService,
            PolopolyDBContext context,
            HttpClient httpClient)
        {
            this.logger = logger;
            this.deliveryClient = contentfulClient;
            this.client = contentfulManagementClient;
            this.fileStorageService = fileStorageService;
            this.context = context;
            this.httpClient = httpClient;
            // ignore parsing errors
            jsonSettings = new JsonSerializerSettings { Error = (se, ev) => { ev.ErrorContext.Handled = true; } };
            InitModuleBuilder();
        }

        public async Task<int?> RunAsync()
        {
            int? typesCreated = 1;

            try
            {
                typesCreated = await CreateContentfulTypes();

                if (typesCreated > 0)
                {
                    await ImportTables();
                }

            }
            catch (Exception ex)
            {
                logger.LogError(ex, "Couldn't create types");
            }

            return typesCreated;
        }

        private async Task<int?> CreateContentfulTypes()
        {
            var assembly = Assembly.GetExecutingAssembly();
            var types = assembly.GetTypes();
            var models = assembly.GetTypes().Where(t => t.BaseType?.Name == "ContentfulBase")?.ToList();
            if (models == null)
            {
                return null;
            }

            foreach (var model in models.Where(m => m.Name != "Entries"))
            {
                var contentType = new ContentType
                {
                    SystemProperties = new SystemProperties
                    {
                        Id = model.Name.ToLower(),
                    },
                    Name = model.Name,
                    Description = model.FullName,
                    DisplayField = "title",
                };

                contentType.Fields = new List<Field>();

                var properties = model.GetProperties();
                foreach (var property in properties.Where(p => p.Name != "Sys"))
                {
                    var stringLengthAttribute = property.GetCustomAttribute<StringLengthAttribute>();

                    var typeName = property.PropertyType.Name;
                    if (typeName.StartsWith("Nullable"))
                    {
                        typeName = property.PropertyType.GenericTypeArguments.FirstOrDefault()?.Name;
                    }

                    var field = new Field
                    {
                        Name = property.Name,
                        Id = property.Name.ToLower(),
                        Type = typeName switch
                        {
                            "String" => stringLengthAttribute?.MaximumLength < 256 ? "Symbol": "Text", 
                            "Boolean" => "Boolean",
                            "DateTime" => "Date",
                            "Int32" or "Int16" or "Short" or "Byte" => "Integer",
                            "Single" or "Float" or "Double" or "Decimal" => "Number",
                            "Enum" => "Array",
                            _ => "Link"
                        },
                    };

                    if (typeName.StartsWith("List")
                        || typeName.StartsWith("ICollection")
                        || typeName.StartsWith("IEnumerable")
                        || typeName.StartsWith("IList"))
                    {
                        field.Type = "Array";
                        var subType = property.PropertyType.GenericTypeArguments.FirstOrDefault()?.Name;
                        field.Items = new Schema
                        {
                            Type = "Link",
                            LinkType = "Entry",
                            Validations = new List<IFieldValidator>
                            {
                                new LinkContentTypeValidator
                                {
                                    ContentTypeIds = new List<string>
                                    {
                                        subType.ToLower()
                                    },
                                    Message = $"Content should be {subType}",
                                }
                            }
                        };
                    }
                    else if (property.GetCustomAttribute(typeof(ImageAttribute)) != null)
                    {
                        field.Type = "Link";
                        field.LinkType = "Asset";
                    }
                    else if (field.Type == "Link")
                    {
                        field.LinkType = "Entry";
                        field.Validations = new List<IFieldValidator>
                        {
                            new LinkContentTypeValidator(message: $"Content should be {property.PropertyType.Name.ToLower()}", property.PropertyType.Name.ToLower())
                        };
                    }

                    field.Required = property.GetCustomAttribute(typeof(RequiredAttribute)) != null;

                    if ((property.GetCustomAttribute(typeof(UniqueAttribute)) != null || property.GetCustomAttribute(typeof(KeyAttribute)) != null) 
                            && new string[] { "Symbol", "Integer", "Number" }.Contains(field.Type))
                    {
                        if (field.Validations == null)
                        {
                            field.Validations = new List<IFieldValidator>();
                        }
                        field.Validations.Add(new UniqueValidator());
                    };

                    contentType.Fields.Add(field);
                }

                await client.CreateOrUpdateContentType(contentType, version: 1);
                await client.ActivateContentType(contentType.SystemProperties.Id, version: 1);

                logger.LogInformation($"Added type {contentType.Name}");
            }

            return models.Count;
        }
    }
}
