﻿using System;
using System.IO;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Azure.Storage;
using Azure.Storage.Blobs;
using Azure.Storage.Blobs.Models;
using JCImport.Services.Interfaces;

namespace JCImport.Services
{
    public class FileStorageService : IFileStorageService
    {
        private readonly string accountName;
        private readonly string containerName;
        private readonly string accountKey;
        private readonly StorageSharedKeyCredential storageCredentials;
        private readonly Regex base64Info = new Regex("data:.*;base64,*", RegexOptions.Compiled);

        public FileStorageService(string accountName, string containerName, string accountKey)
        {
            this.accountName = accountName;
            this.containerName = containerName;
            this.accountKey = accountKey;
            storageCredentials = new StorageSharedKeyCredential(accountName, accountKey);
        }

        public async Task<string> UploadAsync(string base64, string fileName = "", string contentType = "")
        {
            var match = base64Info.Match(base64);
            if (string.IsNullOrEmpty(contentType) || match != null)
            {
                contentType = match.Groups[0].Value.Replace("data:", "").Replace(";base64,", "");
                base64 = base64Info.Replace(base64, "");
            }
           
            var fileContent = Convert.FromBase64String(base64);
            using (var stream = new MemoryStream(fileContent, false))
            {
                return await UploadAsync(stream, fileName, contentType);
            }
        }

        public async Task<string> UploadAsync(Stream stream, string fileName = "", string contentType = "")
        {
            //var blobUri = new Uri($"https://{accountName}.blob.core.windows.net/{containerName}/{Guid.NewGuid()}{fileName}{GetExtensionFromContentType(contentType)}");
            var blobUri = new Uri($"http://127.0.0.1:10000/{accountName}/{containerName}/{Guid.NewGuid()}{fileName}");
            var blobClient = new BlobClient(blobUri, storageCredentials);

            var blobHttpHeader = new BlobHttpHeaders();
            if (!string.IsNullOrEmpty(contentType))
            {
                blobHttpHeader.ContentType = contentType;
            }

            await blobClient.UploadAsync(stream, blobHttpHeader);

            return blobUri.ToString();
        }

        private string GetExtensionFromContentType(string contentType)
        {
            if (string.IsNullOrEmpty(contentType))
            {
                return string.Empty;
            }
            switch (contentType)
            {
                case "image/jpeg":
                    return ".jpeg";
                case "image/png":
                    return ".png";
                case "video/mp4":
                    return ".mp4";
                default:
                    return string.Empty;
            }
        }
    }
}
