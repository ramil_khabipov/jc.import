﻿#define UPLOAD_IMAGES_TO_CONTENTFUL
using Contentful.Core.Models;
using Contentful.Core.Models.Management;
using Contentful.Core.Search;
using JCImport.Extensions;
using JCImport.Models;
using JCImport.Models.Polopoly;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Reflection.Emit;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using File = Contentful.Core.Models.File;
using FileInfo = JCImport.Models.FileInfo;

namespace JCImport.Services
{
    public partial class Importer : IImporter
    {
        const string BaseUrl = "https://www.thejc.com/image/policy:";
        const string SiteUrl = "https://www.thejc.com";
        const string FileBase = "D:/Content";

        private ModuleBuilder mb;

        public void InitModuleBuilder()
        {
            AssemblyName assembly = new AssemblyName(Guid.NewGuid().ToString());
            var ab = AssemblyBuilder.DefineDynamicAssembly(assembly, AssemblyBuilderAccess.Run);
            mb = ab.DefineDynamicModule(assembly.Name);
        }

        private async Task ImportTables()
        {
            Console.WriteLine("Loading categories");
            await LoadCategories();
            Console.WriteLine("Categories loaded");

            Console.WriteLine("Loading articles");
            var articleIds = await context.Contentrefs
                .Join(context.Taginfos, r => r.Minor, t => t.Minor, (r, t) => new { r, t })
                .Join(context.Versioninfos, tr => tr.t.Version, v => v.Version, (tr, v) => new { tr, v })
                .Where(rtv => rtv.tr.t.Major == 1 
                    && rtv.tr.t.Tagid == 1 
                    && rtv.v.Major == 1 
                    && rtv.v.Minor == rtv.tr.r.Minor
                    && rtv.tr.r.Version == rtv.v.Contentrefsversion
                    && rtv.tr.r.Attributeid == 13
                    && rtv.tr.r.Refmajor == 14
                    && rtv.tr.r.Refminor == 541
                && rtv.tr.r.Minor >= 10000 // testing
                )
                .Select(rtv => rtv.tr.r.Minor)
                .Distinct()
                .OrderBy(res => res)
                .Take(10)
                .ToListAsync();

            Console.WriteLine($"max id = {articleIds.Max()}");

            //var articleIds = await context.Components.Where(c => c.Attributeid == (int)Attr.Title && c.Minor >= 412000).Select(c => c.Minor).Distinct().Take(2000).ToListAsync();
            var articles = await LoadArticles(articleIds);
            Console.WriteLine("Articles loaded");

            Console.WriteLine("Start uploading");
            await FillAndUploadEntry(articles);
            Console.WriteLine("End uploading");

            Console.WriteLine("Start publishing");
            await PublishAll();
            Console.WriteLine("End publishing");
        }

        private async Task LoadCategories()
        {
            var l1 = await context.Categories_L1.ToListAsync();
            var l2 = await context.Categories_L2.ToListAsync();
            var l3 = await context.Categories_L3.ToListAsync();
            var categories = new List<Category>();

            var baseCategory = new Category
            {
                Id = 210,
                Title = "Jewish cronicles",
                Sys = new SystemProperties { Id = "210" },
                ChildCategories = l1.Select(c => new Category
                {
                    Sys = new SystemProperties { Id = c.Minor.ToString() }
                }).ToList()
            };

            categories.Add(baseCategory);

            ConvertCategories(categories, l1, l2);
            ConvertCategories(categories, l2, l3);
            ConvertCategories(categories, l3, null);

            await FillAndUploadEntry(categories);
        }

        private static void ConvertCategories(List<Category> categories, IEnumerable<CategoryBase> cats, IEnumerable<CategoryBase> childs)
        {
            foreach (var category in cats)
            {
                categories.Add(new Category
                {
                    Id = category.Minor,
                    Title = category.Longvalue.ToText(),
                    Sys = new SystemProperties { Id = category.Minor.ToString() },
                    ParentCategory = new Category
                    {
                        Sys = new SystemProperties { Id = category.Refminor.ToString() }
                    },
                    ChildCategories = childs == null ? null 
                        : childs.Where(c => c.Refminor == category.Minor).Select(c => new Category
                        {
                            Sys = new SystemProperties { Id = c.Minor.ToString() }
                        })
                        .ToList()
                });
            }
        }

        private async Task PublishAll()
        {
            var failedIds = new List<string>();
            while (true)
            {
                var assets = await client.GetAssetsCollection(new QueryBuilder<Asset>().FieldLessThan(f => f.SystemProperties.PublishedCounter, "1").Limit(1000));
                if (assets.Count() == 0)
                {
                    break;
                }
                foreach (var asset in assets.Where(a => !failedIds.Contains(a.SystemProperties.Id)))
                {
                    Console.WriteLine(asset.SystemProperties.Id);
                    if (asset.SystemProperties.PublishedAt == null)
                    {
                        try
                        {
                            await client.PublishAsset(asset.SystemProperties.Id, asset.SystemProperties.Version.GetValueOrDefault());
                        }
                        catch (Exception ex)
                        {
                            logger.LogError(ex, $"Couldn't publish asset {asset.SystemProperties.Id}");
                            failedIds.Add(asset.SystemProperties.Id);
                        }
                    }
                }
            }
            failedIds = new List<string>();
            while (true)
            {
                var entries = await client.GetEntriesCollection(new QueryBuilder<ContentfulBase>().FieldLessThan(f => f.Sys.PublishedCounter, "1").Limit(1000));
                if (entries.Count() == 0)
                {
                    break;
                }
                foreach (var entry in entries.Where(e => !failedIds.Contains(e.Sys.Id)))
                {
                    Console.WriteLine(entry.Sys.Id);
                    if (entry.Sys.PublishedAt == null)
                    {
                        try
                        {
                            await client.PublishEntry(entry.Sys.Id, entry.Sys.Version.GetValueOrDefault());
                        }
                        catch (Exception ex)
                        {
                            logger.LogError(ex, $"Couldn't publish entry {entry.Sys.Id}");
                            failedIds.Add(entry.Sys.Id);
                        }
                    }
                }
            }
        }

        private async Task<IEnumerable<Article>> LoadArticles(IEnumerable<int> ids)
        {
            var articles = new List<Article>();
            foreach (var id in ids)
            {
                var version = await context.Components.Where(c => c.Minor == id).MaxAsync(c => c.Version);
                var section = await context.Components.Where(c => c.Minor == id && c.Version == version).ToListAsync();

                var dict = section.ToDictionary(s => s.Attributeid, s => s.Longvalue);
                if (!dict.ContainsKey((int)Attr.Body))
                {
                    continue; //let's skip articles without body
                }
                Image titleImage = null;
                List<Article> relatedArticles = null;
                List<Category> categories = null;
                List<Author> authors = null;
                DateTime? editedDate = null;
                DateTime? publishedDate = null;

                if (dict.ContainsKey((int)Attr.Categorization))
                {
                    try
                    {
                        var currentArticle = new Article
                        {
                            Sys = new SystemProperties { Type = "Entry", Id = id.ToString() },
                        };
                        authors = await GetAuthorsForArticle(dict[(int)Attr.Categorization], currentArticle);                        
                    }
                    catch (Exception ex)
                    {
                        logger.LogError(ex, $"Couldn't get authors articles for {id}");
                    }
                }

                if (dict.ContainsKey((int)Attr.HasImages))
                {
                    try
                    {
                        titleImage = await CreateTitleImageForEntry(id);
                    }
                    catch (Exception ex)
                    {
                        logger.LogError(ex, $"Couldn't create image for {id}");
                    }
                }

                if (dict.ContainsKey((int)Attr.HasRelated))
                {
                    try
                    {
                        relatedArticles = await GetRelatedArticlesForArticle(id);
                    }
                    catch (Exception ex)
                    {
                        logger.LogError(ex, $"Couldn't get related articles for {id}");
                    }
                }

                try
                {
                    categories = await GetCategoryForArticle(id);
                }
                catch (Exception ex)
                {
                    logger.LogError(ex, $"Couldn't related articles for {id}");
                }

                int.TryParse(dict.ContainsKey((int)Attr.Rating) ? dict[(int)Attr.Rating] : "0", out int rating);
                int.TryParse(dict.ContainsKey((int)Attr.Priority) ? dict[(int)Attr.Priority] : "0", out int priority);

                if (dict.ContainsKey((int)Attr.EditedDateN)) 
                {
                    editedDate = (new DateTime(1970, 01, 01)).AddMilliseconds(double.Parse(dict[(int)Attr.EditedDateN]));
                }
                else
                {
                    if (DateTime.TryParse(dict.ContainsKey((int)Attr.EditedDate) ? dict[(int)Attr.EditedDate] : null, out DateTime editedDateTime))
                    {
                        editedDate = editedDateTime;
                    }
                }
                
                if (dict.ContainsKey((int)Attr.PublishedDateN))
                {
                    publishedDate = (new DateTime(1970, 01, 01)).AddMilliseconds(double.Parse(dict[(int)Attr.PublishedDateN]));
                }
                else
                {
                    if (DateTime.TryParse(dict.ContainsKey((int)Attr.PublishedDate) ? dict[(int)Attr.PublishedDate] : null, out DateTime publishedDateTime))
                    {
                        publishedDate = publishedDateTime;
                    }
                }

                if (editedDate.HasValue)
                {
                    editedDate = DateTime.SpecifyKind(editedDate.Value, DateTimeKind.Utc);
                }
                if (publishedDate.HasValue)
                {
                    publishedDate = DateTime.SpecifyKind(publishedDate.Value, DateTimeKind.Utc);
                }

                var article = new Article
                {
                    Id = id,
                    Sys = new SystemProperties { Type = "Entry", Id = id.ToString() },
                    Title = dict.ContainsKey((int)Attr.Title) ? dict[(int)Attr.Title].ToText() : dict.ContainsKey((int)Attr.Headline) ? dict[(int)Attr.Headline].ToText() : null,
                    SubTitle = dict.ContainsKey((int)Attr.SubTitle) ? dict[(int)Attr.SubTitle].ToText() : null,
                    Body = dict.ContainsKey((int)Attr.Body) ? await ReplaceImageLinks(dict[(int)Attr.Body].ToText()) : null,
                    ShortTitle = dict.ContainsKey((int)Attr.ShortTitle) ? dict[(int)Attr.ShortTitle].ToText() : null,
                    Rating = rating,
                    Priority = priority,
                    FactsTitle = dict.ContainsKey((int)Attr.FactsTitle) ? dict[(int)Attr.FactsTitle].ToText() : null,
                    FactsBody = dict.ContainsKey((int)Attr.FactsBody) ? dict[(int)Attr.FactsBody].ToText() : null,
                    Author = dict.ContainsKey((int)Attr.Author) ? dict[(int)Attr.Author].ToText() : null,
                    EditedDate = editedDate,
                    PublishedDate = publishedDate,
                    Sponsored = dict.ContainsKey((int)Attr.IsSponsored) ? dict[(int)Attr.IsSponsored].ToLower() == "true" ? true : false : false,
                    Promoted = dict.ContainsKey((int)Attr.IsPromoted) ? dict[(int)Attr.IsPromoted].ToLower() == "true" ? true : false : false,
                    OnlineState = dict.ContainsKey((int)Attr.OnlineState) ? dict[(int)Attr.OnlineState].ToLower() == "true" ? true : false : false,
                    ShowContent = dict.ContainsKey((int)Attr.ShowContent) ? dict[(int)Attr.ShowContent].ToLower() == "true" ? true : false : false,
                    TitleImage = titleImage,
                    RelatedArticles = relatedArticles,
                    Categories = categories,
                    Authors = authors,
                };
                articles.Add(article);

                logger.LogInformation(article.Title, "added");
            }

            return articles;
        }

        private async Task<List<Author>> GetAuthorsForArticle(string categorizationJson, Article article)
        {
            var authors = new List<Author>();
            if (categorizationJson != null)
            {
                var categorizationInfo = JsonConvert.DeserializeObject<CategorizationInfo>(categorizationJson, jsonSettings);
                if (categorizationInfo != null && categorizationInfo.Dimensions != null)
                {
                    var authorsDimensions = categorizationInfo.Dimensions.FirstOrDefault(d => d.Id == "dimension.Author");

                    if (authorsDimensions != null && authorsDimensions.Entities != null)
                    {
                        foreach (var entity in authorsDimensions.Entities)
                        {
                            var authorId = entity.Id;
                            var authorName = entity.Name;
                            var author = await GetOrCreateAuthor(authorId, article);
                            authors.Add(author);
                        }
                    }
                }
            }

            return authors;
        }

        private async Task<Author> GetOrCreateAuthor(string authorId, Article article)
        {
            int? authorSectionId = null;
            if (authorId.StartsWith("1."))
            {
                authorId = authorId.Substring(2);
                if (int.TryParse(authorId, out int numId))
                {
                    authorSectionId = numId;
                }
            }
            else
            {
                authorSectionId = (await context.Externalids.FirstOrDefaultAsync(e => e.ExternalId == authorId))?.Minor;
            }

            if (authorSectionId != null)
            {
                var authorEntry = await client.GetEntriesForLocale(new QueryBuilder<Author>().FieldEquals(f => f.Sys.Id, authorSectionId.ToString()));
                if (authorEntry != null && authorEntry.Count() > 0)
                {
                    var entry = authorEntry.FirstOrDefault();
                    // adding current article to author
                    entry.Articles.Add(article);
                    await FillAndUploadEntry(new object[] { entry });
                    //await client.CreateOrUpdateEntry(entry, entry.Sys.Id);

                    return Author.New(entry.Sys.Id);
                }
                else
                {
                    var version = await context.Components.Where(c => c.Minor == authorSectionId).MaxAsync(c => c.Version);
                    var section = await context.Components.Where(c => c.Minor == authorSectionId && c.Version == version).ToListAsync();

                    var dict = section.ToDictionary(s => s.Attributeid, s => s.Longvalue);
                    Image titleImage = null;
                    if (dict.ContainsKey((int)Attr.HasImages))
                    {
                        try
                        {
                            titleImage = await CreateTitleImageForEntry(authorSectionId.Value);
                        }
                        catch (Exception ex)
                        {
                            logger.LogError(ex, $"Couldn't create image for {authorSectionId}");
                        }
                    }

                    var author = new Author
                    {
                        Id = authorSectionId.Value,
                        Sys = new SystemProperties { Type = "Entry", Id = authorSectionId.Value.ToString() },
                        Title = dict.ContainsKey((int)Attr.Title) ? dict[(int)Attr.Title].ToText() : dict.ContainsKey((int)Attr.Headline) ? dict[(int)Attr.Headline].ToText() : null,
                        Biography = dict.ContainsKey((int)Attr.Biography) ? dict[(int)Attr.Biography].ToText() : null,
                        Email = dict.ContainsKey((int)Attr.Email) ? dict[(int)Attr.Email].ToText() : null,
                        Images = new List<Image> { titleImage },
                        Articles = new List<Article> { article },
                    };

                    await FillAndUploadEntry(new object[] { author });

                    return author;
                }
            }

            return null;
        }

        private async Task<string> ReplaceImageLinks(string html)
        {
            var imgLinks = ExtractLinks(html);
            try
            {
                foreach (var imgLink in imgLinks)
                {

                    var imageId = GetIdFromUrl(imgLink);
                    var url = string.Empty;

                    if (int.TryParse(imageId, out int imgId))
#if UPLOAD_IMAGES_TO_CONTENTFUL
                    {
                        var image = await CreateImage(imgId);
                        if (image != null)
                        {
                            var asset = await client.GetAsset((image.ImageAsset as Reference)?.Sys?.Id);
                            url = asset?.Files["en-US"]?.Url;
                        }
                        else
                        {
                            url = imgLink;
                        }

                    }
#else
                    {
                        var imagePath = $"{SiteUrl}/{imgLink}";
                        var imageBytes = await httpClient.GetByteArrayAsync(imagePath);
                        var bareImgLink = imgLink.Split('?')[0];
                        var imageName = Path.GetFileName(bareImgLink);
                        var imageAsset = await CreateImageAsset(imageBytes, imageName);
                        var asset = await client.GetAsset(imageAsset.Sys.Id);
                        url = asset?.Files["en-US"]?.Url;
                    }
#endif

                    // This replaces old links to url to images, but actually we need to render the page on the fly,
                    // cos we also need to display the title of the image
                    html = html.Replace(imgLink, url);
                }
            }
            catch { };

            return html;
        }

        private string GetIdFromUrl(string imgLink)
        {
            var regex = new Regex(@"\.(?<ID>(?:\d*))[^\d]", RegexOptions.Compiled);

            return regex.Match(imgLink).Groups.GetValueOrDefault("ID")?.Value;
        }

        private IEnumerable<string> ExtractLinks(string html)
        {
            var regex = new Regex(@"<img\b[^>]*?\b(src)\s*=\s*(?:""(?<URL>(?:\\""|[^""])*)"")", RegexOptions.Compiled);

            return regex.Matches(html).Select(m => m.Groups.GetValueOrDefault("URL")?.Value);
        }

        private async Task<List<Category>> GetCategoryForArticle(int id)
        {
            var categoryIndexes = await context.Contentrefs
                .Where(c => c.Minor == id && c.Attributeid == 0)
                .Select(r => r.Refminor)
                .Distinct()
                .ToListAsync();

            if (categoryIndexes.Count() > 0)
            {
                return categoryIndexes.Select(categoryIndex => new Category
                {
                    Sys = new SystemProperties { Id = categoryIndex.ToString(), Type = "Entry" },
                    Id = categoryIndex,
                }).ToList();
            }

            return null;
        }

        private async Task<List<Article>> GetRelatedArticlesForArticle(int id)
        {
            var related = await context.Contentrefs
                .Where(c => c.Minor == id && context.Attributeids.Any(a => a.Attribgroup == "related" && a.Id == c.Attributeid))
                .ToListAsync();

            if (related.Count == 0)
            {
                return null;
            }

            var latestVersion = related.Max(r => r.Version);
            var relatedIndexes = related.Where(r => r.Version == latestVersion).Select(r => r.Refminor);

            if (relatedIndexes.Count() > 0)
            {
                return relatedIndexes.Select(relatedIndex => new Article
                {
                    Sys = new SystemProperties { Id = relatedIndex.ToString(), Type = "Entry" },
                    Id = relatedIndex,
                }).ToList();
            }

            return null;
        }

        private async Task<Image> CreateTitleImageForEntry(int id)
        {
            var imageSectionId = await GetReferenceId(id, (int)Attr.TitleImage);
            if (imageSectionId != null)
            {
                return await CreateImage(imageSectionId.Value);
            }

            return null;
        }

        private async Task<Image> CreateImage(int imageSectionId)
        {
            // let's check if it's already exists
            var imageEntry = await client.GetEntriesCollection(new QueryBuilder<ContentfulBase>().FieldEquals(f => f.Sys.Id, imageSectionId.ToString()));
            if (imageEntry != null && imageEntry.Count() > 0)
            {
                var entry = imageEntry.FirstOrDefault();
                return Image.New(entry.Sys.Id);
            }
            var image = new Image();
            var section = await context.Components.Where(c => c.Minor == imageSectionId
                && c.Version == context.Components.Where(c => c.Minor == imageSectionId).Max(c => c.Version)).ToListAsync();
            var dict = section.ToDictionary(s => s.Attributeid, s => s.Longvalue);
            var fileInfoJson = dict.ContainsKey((int)Attr.FileInfoJson) ? dict[(int)Attr.FileInfoJson] : null;
            if (fileInfoJson != null)
            {
                var fileInfo = JsonConvert.DeserializeObject<FileInfo>(fileInfoJson, jsonSettings);
                if (fileInfo != null)
                {
                    image.Title = fileInfo.Title.ToText();
                    image.CitationText = fileInfo.CitationText.ToText();
                    image.CitationUrl = fileInfo.CitationUrl;
                    image.Description = fileInfo.Description.ToText();
                }
            }
            var imageInfoJson = dict.ContainsKey((int)Attr.ImageInfoJson) ? dict[(int)Attr.ImageInfoJson] : null;
            if (imageInfoJson != null)
            {
                var imageInfo = JsonConvert.DeserializeObject<ImageInfo>(imageInfoJson, jsonSettings);
                if (imageInfo != null)
                {
                    image.Sys = new SystemProperties { Id = imageSectionId.ToString(), Type = "Entry" };
                    image.Id = imageSectionId;
                    image.Width = imageInfo.ImageWidth;
                    image.Height = imageInfo.ImageHeight;
                    var filePath = GetFileFromUri(imageInfo.FileUri);
                    if (!string.IsNullOrEmpty(filePath))
                    {
#if UPLOAD_IMAGES_TO_CONTENTFUL
                        // -- upload image to contentful
                        image.ImageAsset = await UploadImage(filePath);
#else
                        // -- upolad to Azure blobs
                        image.Url = await UploadFile(filePath);
#endif
                    }

                    await FillAndUploadEntry(new List<Image> { image });

                    return image;
                }
            }
            else
            {
                var imageNameJson = dict.ContainsKey((int)Attr.FileNameJson) ? dict[(int)Attr.FileNameJson] : null;
                if (imageNameJson != null)
                {
                    var fileName = JsonConvert.DeserializeObject<FileName>(imageNameJson, jsonSettings);
                    if (fileName != null)
                    {
                        image.Sys = new SystemProperties { Id = imageSectionId.ToString(), Type = "Entry" };
                        image.Id = imageSectionId;
                        image.Width = fileName.Width;
                        image.Height = fileName.Height;
                        var fileNameJson = dict.ContainsKey((int)Attr.FileNameJson) ? dict[(int)Attr.FileNameJson] : null;
                        var imageName = fileName.FilePath;
                        var filePath = GetFilePath(imageSectionId, imageName);
                        if (!string.IsNullOrEmpty(filePath))
                        {
#if UPLOAD_IMAGES_TO_CONTENTFUL
                            image.ImageAsset = await UploadImage(filePath);
                            var imagePath = $"{BaseUrl}1.{imageSectionId}/image/{imageName}";
                            var imageBytes = await httpClient.GetByteArrayAsync(imagePath);
                            image.ImageAsset = await CreateImageAsset(imageBytes, imageName);
#else
                            image.Url = await UploadFile(filePath);
#endif
                        }

                        await FillAndUploadEntry(new List<Image> { image });

                        return image;
                    }
                }
            }

            return null;
        }

        private async Task<string> UploadFile(string filePath)
        {
            try
            {
                var imageStream = new FileStream(path: filePath, FileMode.Open);
                var imageName = Path.GetFileName(filePath);
                if (imageName.Length > 100)
                {
                    imageName = imageName.Remove(100);
                }
                var url = await fileStorageService.UploadAsync(stream: imageStream, fileName: imageName);

                return url;
            }
            catch (Exception ex)
            {
                logger.LogError(ex, $"Failed to upload the file {filePath}");
            }

            return null;
        }

        private string GetFileFromUri(string fileUri)
        {
            var regex = new Regex(@"-(?<ID>(?:\d*))/(?<PATH>(?:.*))", RegexOptions.Compiled);

            var stringId = regex.Match(fileUri).Groups.GetValueOrDefault("ID")?.Value;
            if (stringId.Length < 5)
            {
                return string.Empty;
            }

            var p1 = stringId.Substring(0, 2);
            var p2 = stringId.Substring(2, 2);
            var p3 = stringId.Substring(4);

            var pathToFile = regex.Match(fileUri).Groups.GetValueOrDefault("PATH")?.Value;

            var pathPrefix = $"{FileBase}/c8/1-/{p1}/{p2}/{p3}";
            if (!Directory.Exists(pathPrefix))
            {
                pathPrefix = pathPrefix.Replace("c8", "c7");
                if (!Directory.Exists(pathPrefix))
                {
                    return string.Empty;
                }
            }
            var fileName = Path.Combine(pathPrefix, pathToFile);

            if (!System.IO.File.Exists(fileName))
            {
                return string.Empty;
            }

            return fileName;
        }

        private string GetFilePath(int imageSectionId, string imageName)
        {
            var stringId = imageSectionId.ToString();
            if (stringId.Length < 5)
            {
                return string.Empty;
            }

            var p1 = stringId.Substring(0, 2);
            var p2 = stringId.Substring(2, 2);
            var p3 = stringId.Substring(4);

            var pathPrefix = $"{FileBase}/c8/1-/{p1}/{p2}/{p3}";
            if (!Directory.Exists(pathPrefix))
            {
                pathPrefix = pathPrefix.Replace("c8", "c7");
                if (!Directory.Exists(pathPrefix))
                {
                    return string.Empty;
                }
            }

            var fileName = Path.Combine(pathPrefix, Regex.Replace(imageName.Replace("#005cu0027", "-").Replace("/-", "/").Trim('-'), "[^a-zA-Z0-9/]+", "-"));

            if (!System.IO.File.Exists(fileName))
            {
                if (System.IO.File.Exists(fileName + "-1"))
                {
                    return fileName + "-1";
                }
                else if(System.IO.File.Exists(fileName + "-2"))
                {
                    return fileName + "-2";
                }
                else if (System.IO.File.Exists(fileName + "-3"))
                {
                    return fileName + "-4";
                }

                return string.Empty;
            }

            return fileName;
        }

        private async Task<Reference> CreateImageAsset(byte[] imageBytes, string fileName)
        {
            var managementAsset = NewManagementAsset(fileName);

            return await CreateImageFromAsset(imageBytes, fileName, managementAsset);
        }

        private static ManagementAsset NewManagementAsset(string fileName)
        {
            return new ManagementAsset
            {
                SystemProperties = new SystemProperties { Id = Guid.NewGuid().ToString() },
                Title = new Dictionary<string, string> {
                    { "en-US", fileName }
                },
                Files = new Dictionary<string, File>
                {
                    { "en-US", new File() {
                            ContentType = "image/jpeg",
                            FileName = fileName,
                        }
                    }
                }
            };
        }

        private async Task<Reference> CreateImageFromAsset(byte[] imageBytes, string fileName, ManagementAsset managementAsset)
        {
            try
            {
                var asset = await client.UploadFileAndCreateAsset(managementAsset, imageBytes);

                return new Reference
                {
                    Sys = new ReferenceProperties
                    {
                        LinkType = SystemLinkTypes.Asset,
                        Id = asset.SystemProperties.Id,
                    }
                };
            }
            catch (Exception ex)
            {
                logger.LogInformation(ex, $"Couldn't load image by url {fileName}");
            }

            return null;
        }

        private async Task<Reference> UploadImage(string path)
        {
            var imageBytes = System.IO.File.ReadAllBytes(path);
            var imageName = Path.GetFileName(path);

            return await CreateImageAsset(imageBytes, imageName);
        }

        private async Task<int?> GetReferenceId(int id, int titleImageAttr)
        {
            return (await context.Contentrefs.OrderByDescending(c => c.Version)
                    .FirstOrDefaultAsync(c => c.Minor == id && c.Attributeid == titleImageAttr))?.Refminor;
        }

        private async Task FillAndUploadEntry(IEnumerable<object> tables)
        {
            foreach (var table in tables)
            {
                var tableType = table.GetType();
                var entry = new Entry<dynamic>();
                var tableName = tableType.Name.ToLower();

                var properties = tableType.GetProperties().Where(p => p.Name != "Sys");
                var tb = mb.DefineType($"Dynamic{Guid.NewGuid()}", TypeAttributes.Public);

                foreach (var property in properties)
                {
                    var propertyName = property.Name.ToLower();

                    var fieldType = typeof(Dictionary<string, dynamic>);

                    tb.DefineField(propertyName, fieldType, FieldAttributes.Public);
                }

                var objectType = tb.CreateType();
                var objectInstance = Activator.CreateInstance(objectType);

                foreach (var property in properties)
                {
                    var propertyName = property.Name.ToLower();
                    var propertyType = property.PropertyType;
                    var propertyValue = property.GetValue(table);
                    if (propertyType.Name == "String")
                    {
                        var stringLength = property.GetCustomAttribute<StringLengthAttribute>()?.MaximumLength ?? 50000;
                        if ((propertyValue as string)?.Length > stringLength)
                        {
                            logger.LogWarning($"{propertyName} has been cropped", propertyValue);
                            propertyValue = (propertyValue as string).Remove(stringLength);
                        }
                    }
                    if (propertyType.FullName.Contains("DateTime") && propertyValue != null)
                    {
                        propertyValue = ((DateTime)propertyValue).ToString("s", CultureInfo.CreateSpecificCulture("en-US")) +"Z";
                    }
                    if (propertyValue != null)
                    {
                        try
                        {
                            if (propertyType.Name.StartsWith("ICollection")
                                 || propertyType.Name.StartsWith("IList")
                                 || propertyType.Name.StartsWith("IEnumerable")
                                 || propertyType.Name.StartsWith("List"))
                            {
                                var references = ContentfulBase.CreateReferences(propertyValue as IEnumerable<ContentfulBase>);

                                propertyValue = references;
                            }
                            else
                            {
                                // in case if this is Entry
                                var reference = ContentfulBase.CreateReference(propertyValue);
                                if (reference?.Sys != null)
                                {
                                    propertyValue = reference;
                                }
                            }
                        }
                        catch { };
                    }

                    objectType.InvokeMember(propertyName, BindingFlags.SetField, null, objectInstance,
                        new object[] { new Dictionary<string, dynamic> { { "en-US", propertyValue } } });

                }

                entry.Fields = objectInstance;
                var systemProperties = (table as ContentfulBase).Sys;
                if (systemProperties != null)
                {
                    entry.SystemProperties = new SystemProperties
                    {
                        Id = systemProperties.Id,
                        Type = systemProperties.Type,
                        Version = systemProperties.Version,
                    };
                }
                try
                {
                    var newEntry = await client.CreateOrUpdateEntry(entry, contentTypeId: tableName, version: entry.SystemProperties.Version);
                    Console.WriteLine($"-> {newEntry.SystemProperties.Id}");
                }
                catch (Exception ex)
                {
                    if (ex.Message.Contains("server")) // let's retry
                    {
                        try
                        {
                            Thread.Sleep(3000);
                            await client.CreateOrUpdateEntry(entry, contentTypeId: tableName, version: entry.SystemProperties.Version);
                        }
                        catch (Exception ex2)
                        {
                            Console.WriteLine($"(Retry) Error uploading entry {entry.SystemProperties.Id}:");
                            Console.WriteLine(ex2.Message);
                        }
                    }
                    Console.WriteLine($"Error uploading entry {entry.SystemProperties.Id}:");
                    Console.WriteLine(ex.Message);
                }
            }
        }
    }
}
