﻿using System;
using JCImport.Models.Polopoly;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

#nullable disable

namespace JCImport.Domain
{
    public partial class PolopolyDBContext : DbContext
    {
        public PolopolyDBContext()
        {
        }

        public PolopolyDBContext(DbContextOptions<PolopolyDBContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Acl> Acls { get; set; }
        public virtual DbSet<AclDatum> AclData { get; set; }
        public virtual DbSet<AclOwner> AclOwners { get; set; }
        public virtual DbSet<Attributeid> Attributeids { get; set; }
        public virtual DbSet<Component> Components { get; set; }
        public virtual DbSet<Contentfile> Contentfiles { get; set; }
        public virtual DbSet<Contentlock> Contentlocks { get; set; }
        public virtual DbSet<Contentref> Contentrefs { get; set; }
        public virtual DbSet<Eventinfo> Eventinfos { get; set; }
        public virtual DbSet<Eventinfoqueue> Eventinfoqueues { get; set; }
        public virtual DbSet<Externalid> Externalids { get; set; }
        public virtual DbSet<GroupDatum> GroupData { get; set; }
        public virtual DbSet<GroupMember> GroupMembers { get; set; }
        public virtual DbSet<GroupOwner> GroupOwners { get; set; }
        public virtual DbSet<Idcounter> Idcounters { get; set; }
        public virtual DbSet<Installationinfo> Installationinfos { get; set; }
        public virtual DbSet<JmsMessage> JmsMessages { get; set; }
        public virtual DbSet<JmsTransaction> JmsTransactions { get; set; }
        public virtual DbSet<Majorinfo> Majorinfos { get; set; }
        public virtual DbSet<Minorinfo> Minorinfos { get; set; }
        public virtual DbSet<Registereduser> Registeredusers { get; set; }
        public virtual DbSet<Sessioninfo> Sessioninfos { get; set; }
        public virtual DbSet<Tagid> Tagids { get; set; }
        public virtual DbSet<Taginfo> Taginfos { get; set; }
        public virtual DbSet<Versioninfo> Versioninfos { get; set; }
        public virtual DbSet<Category_L1> Categories_L1 { get; set; }
        public virtual DbSet<Category_L2> Categories_L2 { get; set; }
        public virtual DbSet<Category_L3> Categories_L3 { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.HasAnnotation("Relational:Collation", "Latin1_General_CS_AS");

            modelBuilder.Entity<Acl>(entity =>
            {
                entity.Property(e => e.Permission).IsUnicode(false);

                entity.Property(e => e.PrincipalId).IsUnicode(false);
            });

            modelBuilder.Entity<AclDatum>(entity =>
            {
                entity.Property(e => e.FirstOwnerId).IsUnicode(false);

                entity.Property(e => e.Name).IsUnicode(false);
            });

            modelBuilder.Entity<AclOwner>(entity =>
            {
                entity.Property(e => e.PrincipalId).IsUnicode(false);
            });

            modelBuilder.Entity<Attributeid>(entity =>
            {
                entity.Property(e => e.Attribgroup).IsUnicode(false);

                entity.Property(e => e.Name).IsUnicode(false);
            });

            modelBuilder.Entity<Component>(entity =>
            {
                entity.Property(e => e.Value).IsUnicode(false);
            });

            modelBuilder.Entity<Contentfile>(entity =>
            {
                entity.Property(e => e.Path).IsUnicode(false);
            });

            modelBuilder.Entity<Contentlock>(entity =>
            {
                entity.Property(e => e.Sessionkey).IsUnicode(false);

                entity.Property(e => e.Userid).IsUnicode(false);
            });

            modelBuilder.Entity<Eventinfo>(entity =>
            {
                entity.Property(e => e.Id).ValueGeneratedNever();
            });

            modelBuilder.Entity<Externalid>(entity =>
            {
                entity.Property(e => e.ExternalId).IsUnicode(false);
            });

            modelBuilder.Entity<GroupDatum>(entity =>
            {
                entity.Property(e => e.FirstOwnerId).IsUnicode(false);

                entity.Property(e => e.LdapGroupDn).IsUnicode(false);

                entity.Property(e => e.Name).IsUnicode(false);
            });

            modelBuilder.Entity<GroupMember>(entity =>
            {
                entity.Property(e => e.PrincipalId).IsUnicode(false);
            });

            modelBuilder.Entity<GroupOwner>(entity =>
            {
                entity.Property(e => e.PrincipalId).IsUnicode(false);
            });

            modelBuilder.Entity<Idcounter>(entity =>
            {
                entity.Property(e => e.Id).ValueGeneratedNever();
            });

            modelBuilder.Entity<JmsMessage>(entity =>
            {
                entity.HasIndex(e => new { e.Messageid, e.Destination }, "JMS_MESSAGES_IDX")
                    .IsUnique()
                    .IsClustered();

                entity.Property(e => e.Destination).IsUnicode(false);

                entity.Property(e => e.Txop)
                    .IsUnicode(false)
                    .IsFixedLength(true);
            });

            modelBuilder.Entity<JmsTransaction>(entity =>
            {
                entity.HasKey(e => e.Txid)
                    .HasName("PK__JMS_TRAN__827BEF0E750D36D2");

                entity.Property(e => e.Txid).ValueGeneratedNever();
            });

            modelBuilder.Entity<Majorinfo>(entity =>
            {
                entity.Property(e => e.Contentclassname).IsUnicode(false);

                entity.Property(e => e.Contenthomeclassname).IsUnicode(false);

                entity.Property(e => e.Contenthomejndiname).IsUnicode(false);

                entity.Property(e => e.Majorname).IsUnicode(false);
            });

            modelBuilder.Entity<Registereduser>(entity =>
            {
                entity.Property(e => e.Active).HasDefaultValueSql("((0))");

                entity.Property(e => e.Isldapuser).HasDefaultValueSql("((0))");

                entity.Property(e => e.Lastlogintime).HasDefaultValueSql("((0))");

                entity.Property(e => e.Loginname).IsUnicode(false);

                entity.Property(e => e.Numlogins).HasDefaultValueSql("((0))");

                entity.Property(e => e.Passwordhash).IsUnicode(false);
            });

            modelBuilder.Entity<Sessioninfo>(entity =>
            {
                entity.Property(e => e.Loginname).IsUnicode(false);

                entity.Property(e => e.Sessionkey).IsUnicode(false);
            });

            modelBuilder.Entity<Tagid>(entity =>
            {
                entity.Property(e => e.Name).IsUnicode(false);
            });

            modelBuilder.Entity<Versioninfo>(entity =>
            {
                entity.Property(e => e.Approvedby).IsUnicode(false);

                entity.Property(e => e.Committedby).IsUnicode(false);

                entity.Property(e => e.Createdby).IsUnicode(false);
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
