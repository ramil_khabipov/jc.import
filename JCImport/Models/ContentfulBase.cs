﻿using Contentful.Core.Models;
using Contentful.Core.Models.Management;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JCImport.Models
{
    public class ContentfulBase
    {
        [NotMapped]
        public SystemProperties Sys { get; set; }

        internal static Reference CreateReference(object asset)
        {
            var contentfulBase = asset as ContentfulBase;

            if (contentfulBase != null)
            {
                return new Reference(linkType: SystemLinkTypes.Entry, id: contentfulBase.Sys?.Id);
            }

            return null;
        }

        internal static IEnumerable<object> CreateReferences(IEnumerable<ContentfulBase> entries)
        {
            try
            {
                return entries.Select(v => new Reference(linkType: SystemLinkTypes.Entry, id: v?.Sys?.Id)).ToList();
            }
            catch
            {
                return null;
            }
        }
    }
}
