﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace JCImport.Models
{
    public class Article : ContentfulBase
    {
        public int Id { get; set; }
        [StringLength(255)]
        public string Title { get; internal set; }
        [StringLength(500)]
        public string SubTitle { get; internal set; }
        [StringLength(255)]
        public string ShortTitle { get; internal set; }
        public string Body { get; internal set; }
        [StringLength(255)]
        public string FactsTitle { get; internal set; }
        public string FactsBody { get; internal set; }
        public string PdfText { get; set; }
        public int ViewsCount { get; set; }
        public DateTime? EditedDate { get; set; }
        public DateTime? PublishedDate { get; set; }
        public Image TitleImage { get; set; }
        public ICollection<Author> Authors { get; set; }
        public ICollection<Category> Categories { get; set; }
        public ICollection<Article> RelatedArticles { get; set; }
        public int Rating { get; set; }
        public int RatingCount { get; set; }
        public int Priority { get; set; }
        public bool Sponsored { get; set; }
        public bool Promoted { get; set; }
        public bool OnlineState { get; set; }
        public bool ShowContent { get; set; }
        [StringLength(255)]
        public string Author { get; set; }
    }
}
