﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace JCImport.Models.Polopoly
{
    [Keyless]
    [Table("l2_categories", Schema = "export")]
    public class Category_L2 : CategoryBase
    {
    }
}
