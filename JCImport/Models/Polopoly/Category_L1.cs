﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace JCImport.Models.Polopoly
{
    [Keyless]
    [Table("l1_categories", Schema = "export")]
    public class Category_L1 : CategoryBase
    {
    }
}
