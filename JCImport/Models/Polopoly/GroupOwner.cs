﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

#nullable disable

namespace JCImport.Models.Polopoly
{
    [Keyless]
    [Table("groupOwner")]
    [Index(nameof(GroupId), Name = "groupOwnersIdIdx")]
    [Index(nameof(PrincipalId), Name = "groupOwnersOwnIdx")]
    public partial class GroupOwner
    {
        [Column("groupId")]
        public int GroupId { get; set; }
        [Required]
        [Column("principalId")]
        [StringLength(32)]
        public string PrincipalId { get; set; }
    }
}
