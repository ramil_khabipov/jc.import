﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

#nullable disable

namespace JCImport.Models.Polopoly
{
    [Keyless]
    [Table("groupData")]
    [Index(nameof(Id), Name = "groupIndex", IsUnique = true)]
    public partial class GroupDatum
    {
        [Column("id")]
        public int Id { get; set; }
        [Column("name")]
        [StringLength(32)]
        public string Name { get; set; }
        [Column("creationTime")]
        public int CreationTime { get; set; }
        [Column("firstOwnerId")]
        [StringLength(32)]
        public string FirstOwnerId { get; set; }
        [Column("ldapGroupDn")]
        [StringLength(255)]
        public string LdapGroupDn { get; set; }
    }
}
