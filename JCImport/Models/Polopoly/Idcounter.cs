﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

#nullable disable

namespace JCImport.Models.Polopoly
{
    [Table("idcounters")]
    public partial class Idcounter
    {
        [Key]
        [Column("id")]
        public int Id { get; set; }
        [Column("nextid")]
        public long Nextid { get; set; }
    }
}
