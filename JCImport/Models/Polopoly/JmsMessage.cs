﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

#nullable disable

namespace JCImport.Models.Polopoly
{
    [Keyless]
    [Table("JMS_MESSAGES")]
    [Index(nameof(Destination), Name = "JMS_MESSAGES_DESTINATION")]
    [Index(nameof(Txop), nameof(Txid), Name = "JMS_MESSAGES_TXOP_TXID")]
    public partial class JmsMessage
    {
        [Column("MESSAGEID")]
        public int Messageid { get; set; }
        [Required]
        [Column("DESTINATION")]
        [StringLength(150)]
        public string Destination { get; set; }
        [Column("TXID")]
        public int? Txid { get; set; }
        [Column("TXOP")]
        [StringLength(1)]
        public string Txop { get; set; }
        [Column("MESSAGEBLOB", TypeName = "image")]
        public byte[] Messageblob { get; set; }
    }
}
