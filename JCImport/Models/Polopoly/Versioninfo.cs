﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

#nullable disable

namespace JCImport.Models.Polopoly
{
    [Keyless]
    [Table("versioninfo")]
    [Index(nameof(Major), nameof(Minor), nameof(Version), Name = "versioninfoIndex", IsUnique = true)]
    [Index(nameof(Major), nameof(Committed), nameof(Version), Name = "viChangeIdx")]
    [Index(nameof(Committedby), nameof(Committed), Name = "viCommittedByIdx")]
    [Index(nameof(Componentsversion), nameof(Major), nameof(Minor), Name = "viComponentsIdx")]
    [Index(nameof(Contentrefsversion), nameof(Major), nameof(Minor), Name = "viContentrefsIdx")]
    public partial class Versioninfo
    {
        [Column("major")]
        public int Major { get; set; }
        [Column("minor")]
        public int Minor { get; set; }
        [Column("version")]
        public int Version { get; set; }
        [Column("createdonbranch")]
        public int Createdonbranch { get; set; }
        [Column("previousversion")]
        public int Previousversion { get; set; }
        [Column("componentsversion")]
        public int Componentsversion { get; set; }
        [Column("contentrefsversion")]
        public int Contentrefsversion { get; set; }
        [Column("filesversion")]
        public int Filesversion { get; set; }
        [Column("committed")]
        public int Committed { get; set; }
        [Column("workflowapproved")]
        public int Workflowapproved { get; set; }
        [Column("createdby")]
        [StringLength(32)]
        public string Createdby { get; set; }
        [Column("committedby")]
        [StringLength(32)]
        public string Committedby { get; set; }
        [Column("approvedby")]
        [StringLength(32)]
        public string Approvedby { get; set; }
    }
}
