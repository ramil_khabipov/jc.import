﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

#nullable disable

namespace JCImport.Models.Polopoly
{
    [Keyless]
    [Table("acl")]
    [Index(nameof(AclId), Name = "aclIdIndex")]
    [Index(nameof(PrincipalId), Name = "aclPrincipalIndex")]
    public partial class Acl
    {
        [Column("aclId")]
        public int AclId { get; set; }
        [Required]
        [Column("principalId")]
        [StringLength(32)]
        public string PrincipalId { get; set; }
        [Required]
        [Column("permission")]
        [StringLength(32)]
        public string Permission { get; set; }
    }
}
