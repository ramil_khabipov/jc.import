﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

#nullable disable

namespace JCImport.Models.Polopoly
{
    [Keyless]
    [Table("component")]
    [Index(nameof(Attributeid), Name = "componentAttrIdx")]
    [Index(nameof(Major), nameof(Minor), nameof(Version), nameof(Attributeid), Name = "componentIndex", IsUnique = true)]
    [Index(nameof(Value), nameof(Attributeid), nameof(Major), Name = "componentValIdx")]
    public partial class Component
    {
        [Column("major")]
        public int Major { get; set; }
        [Column("minor")]
        public int Minor { get; set; }
        [Column("version")]
        public int Version { get; set; }
        [Column("attributeid")]
        public int Attributeid { get; set; }
        [Required]
        [Column("value")]
        [StringLength(128)]
        public string Value { get; set; }
        [Required]
        [Column("longvalue", TypeName = "text")]
        public string Longvalue { get; set; }
    }
}
