﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

#nullable disable

namespace JCImport.Models.Polopoly
{
    [Keyless]
    [Table("contentref")]
    [Index(nameof(Major), nameof(Minor), nameof(Version), nameof(Attributeid), Name = "contentRefIdx", IsUnique = true)]
    [Index(nameof(Attributeid), nameof(Major), Name = "contentrefIdIdx")]
    [Index(nameof(Refmajor), nameof(Refminor), nameof(Attributeid), nameof(Refversion), Name = "contentrefRefIdx")]
    public partial class Contentref
    {
        [Column("major")]
        public int Major { get; set; }
        [Column("minor")]
        public int Minor { get; set; }
        [Column("version")]
        public int Version { get; set; }
        [Column("attributeid")]
        public int Attributeid { get; set; }
        [Column("refmajor")]
        public int Refmajor { get; set; }
        [Column("refminor")]
        public int Refminor { get; set; }
        [Column("refversion")]
        public int Refversion { get; set; }
    }
}
