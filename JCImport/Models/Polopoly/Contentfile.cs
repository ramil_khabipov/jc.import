﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

#nullable disable

namespace JCImport.Models.Polopoly
{
    [Keyless]
    [Table("contentfile")]
    [Index(nameof(Major), nameof(Minor), nameof(Version), nameof(Path), Name = "contentfileIdx", IsUnique = true)]
    public partial class Contentfile
    {
        [Column("major")]
        public int Major { get; set; }
        [Column("minor")]
        public int Minor { get; set; }
        [Column("version")]
        public int Version { get; set; }
        [Required]
        [Column("path")]
        [StringLength(255)]
        public string Path { get; set; }
        [Column("filedata")]
        public byte[] Filedata { get; set; }
        [Column("lastmodified")]
        public int Lastmodified { get; set; }
    }
}
