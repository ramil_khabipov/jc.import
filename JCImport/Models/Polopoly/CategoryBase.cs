﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace JCImport.Models.Polopoly
{
    public class CategoryBase
    {
        [Column("major")]
        public int Major { get; set; }
        [Column("minor")]
        public int Minor { get; set; }
        [Column("value")]
        [Required]
        [StringLength(128)]
        public string Value { get; set; }
        [Required]
        [Column("longvalue", TypeName = "text")]
        public string Longvalue { get; set; }
        [Column("refmajor")]
        public int Refmajor { get; set; }
        [Column("refminor")]
        public int Refminor { get; set; }
    }
}
