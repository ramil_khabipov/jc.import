﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

#nullable disable

namespace JCImport.Models.Polopoly
{
    [Keyless]
    [Table("installationinfo")]
    [Index(nameof(Generation), nameof(Major), nameof(Minor), Name = "iiVersionIdx", IsUnique = true)]
    public partial class Installationinfo
    {
        [Column("generation")]
        public int Generation { get; set; }
        [Column("major")]
        public int Major { get; set; }
        [Column("minor")]
        public int Minor { get; set; }
        [Column("timestamp")]
        public int Timestamp { get; set; }
    }
}
