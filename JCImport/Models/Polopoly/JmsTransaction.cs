﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

#nullable disable

namespace JCImport.Models.Polopoly
{
    [Table("JMS_TRANSACTIONS")]
    public partial class JmsTransaction
    {
        [Key]
        [Column("TXID")]
        public int Txid { get; set; }
    }
}
