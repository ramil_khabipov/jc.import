﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

#nullable disable

namespace JCImport.Models.Polopoly
{
    [Keyless]
    [Table("registeredusers")]
    [Index(nameof(Loginname), Name = "regloginnameIdx", IsUnique = true)]
    public partial class Registereduser
    {
        [Required]
        [Column("loginname")]
        [StringLength(64)]
        public string Loginname { get; set; }
        [Required]
        [Column("passwordhash")]
        [StringLength(64)]
        public string Passwordhash { get; set; }
        [Column("regtime")]
        public int Regtime { get; set; }
        [Column("isldapuser")]
        public int? Isldapuser { get; set; }
        [Column("lastlogintime")]
        public int? Lastlogintime { get; set; }
        [Column("numlogins")]
        public int? Numlogins { get; set; }
        [Column("active")]
        public int? Active { get; set; }
    }
}
