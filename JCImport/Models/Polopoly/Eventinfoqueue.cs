﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

#nullable disable

namespace JCImport.Models.Polopoly
{
    [Table("eventinfoqueue")]
    public partial class Eventinfoqueue
    {
        [Key]
        [Column("id")]
        public int Id { get; set; }
        [Column("eventtype")]
        public int Eventtype { get; set; }
        [Column("viewid")]
        public short? Viewid { get; set; }
        [Column("major")]
        public int Major { get; set; }
        [Column("minor")]
        public int Minor { get; set; }
        [Column("version")]
        public int? Version { get; set; }
        [Column("timestamp")]
        public long Timestamp { get; set; }
    }
}
