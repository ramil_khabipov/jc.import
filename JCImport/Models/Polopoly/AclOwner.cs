﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

#nullable disable

namespace JCImport.Models.Polopoly
{
    [Keyless]
    [Table("aclOwner")]
    [Index(nameof(AclId), Name = "aclOwnersIdIndex")]
    [Index(nameof(PrincipalId), Name = "aclOwnersOwnerIdx")]
    public partial class AclOwner
    {
        [Column("aclId")]
        public int AclId { get; set; }
        [Required]
        [Column("principalId")]
        [StringLength(32)]
        public string PrincipalId { get; set; }
    }
}
