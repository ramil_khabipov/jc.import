﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

#nullable disable

namespace JCImport.Models.Polopoly
{
    [Keyless]
    [Table("externalids")]
    [Index(nameof(ExternalId), Name = "externalIdIdx", IsUnique = true)]
    [Index(nameof(Major), nameof(Minor), nameof(ExternalId), Name = "majMinExtIdIdx")]
    public partial class Externalid
    {
        [Column("major")]
        public int Major { get; set; }
        [Column("minor")]
        public int Minor { get; set; }
        [Required]
        [Column("externalid")]
        [StringLength(128)]
        public string ExternalId { get; set; }
    }
}
