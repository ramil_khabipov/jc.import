﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

#nullable disable

namespace JCImport.Models.Polopoly
{
    [Keyless]
    [Table("majorinfo")]
    [Index(nameof(Major), Name = "majorinfoIndex", IsUnique = true)]
    [Index(nameof(Majorname), Name = "majorinfoNameIdx", IsUnique = true)]
    public partial class Majorinfo
    {
        [Column("major")]
        public int Major { get; set; }
        [Column("nextminor")]
        public int? Nextminor { get; set; }
        [Required]
        [Column("majorname")]
        [StringLength(32)]
        public string Majorname { get; set; }
        [Required]
        [Column("contenthomejndiname")]
        [StringLength(32)]
        public string Contenthomejndiname { get; set; }
        [Required]
        [Column("contenthomeclassname")]
        [StringLength(128)]
        public string Contenthomeclassname { get; set; }
        [Required]
        [Column("contentclassname")]
        [StringLength(128)]
        public string Contentclassname { get; set; }
    }
}
