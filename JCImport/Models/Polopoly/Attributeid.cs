﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

#nullable disable

namespace JCImport.Models.Polopoly
{
    [Keyless]
    [Table("attributeid")]
    [Index(nameof(Id), Name = "attridIdx", IsUnique = true)]
    [Index(nameof(Attribgroup), nameof(Name), Name = "attridNameIdx", IsUnique = true)]
    public partial class Attributeid
    {
        [Column("id")]
        public int Id { get; set; }
        [Required]
        [Column("attribgroup")]
        [StringLength(128)]
        public string Attribgroup { get; set; }
        [Required]
        [Column("name")]
        [StringLength(128)]
        public string Name { get; set; }
    }
}
