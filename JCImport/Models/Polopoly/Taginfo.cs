﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

#nullable disable

namespace JCImport.Models.Polopoly
{
    [Keyless]
    [Table("taginfo")]
    [Index(nameof(Major), nameof(Minor), nameof(Tagid), Name = "taginfoIdIndex", IsUnique = true)]
    [Index(nameof(Tagid), Name = "taginfoIndex")]
    public partial class Taginfo
    {
        [Column("major")]
        public int Major { get; set; }
        [Column("minor")]
        public int Minor { get; set; }
        [Column("tagid")]
        public int Tagid { get; set; }
        [Column("version")]
        public int Version { get; set; }
    }
}
