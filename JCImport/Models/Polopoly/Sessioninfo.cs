﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

#nullable disable

namespace JCImport.Models.Polopoly
{
    [Keyless]
    [Table("sessioninfo")]
    [Index(nameof(Loginname), Name = "sessionInfoIdx")]
    public partial class Sessioninfo
    {
        [Required]
        [Column("loginname")]
        [StringLength(64)]
        public string Loginname { get; set; }
        [Required]
        [Column("sessionkey")]
        [StringLength(128)]
        public string Sessionkey { get; set; }
        [Column("expiretime")]
        public int Expiretime { get; set; }
    }
}
