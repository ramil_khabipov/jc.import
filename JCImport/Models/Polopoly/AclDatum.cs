﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

#nullable disable

namespace JCImport.Models.Polopoly
{
    [Keyless]
    [Table("aclData")]
    [Index(nameof(Id), Name = "aclIndex", IsUnique = true)]
    public partial class AclDatum
    {
        [Column("id")]
        public int Id { get; set; }
        [Column("name")]
        [StringLength(32)]
        public string Name { get; set; }
        [Column("creationTime")]
        public int CreationTime { get; set; }
        [Column("firstOwnerId")]
        [StringLength(32)]
        public string FirstOwnerId { get; set; }
    }
}
