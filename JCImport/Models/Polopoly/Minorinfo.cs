﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

#nullable disable

namespace JCImport.Models.Polopoly
{
    [Keyless]
    [Table("minorinfo")]
    [Index(nameof(Major), nameof(Minor), nameof(Branch), Name = "minorInfoBranchIdx", IsUnique = true)]
    [Index(nameof(Latestcommitted), nameof(Major), nameof(Minor), Name = "minorinfLatestIdx")]
    public partial class Minorinfo
    {
        [Column("major")]
        public int Major { get; set; }
        [Column("minor")]
        public int Minor { get; set; }
        [Column("branch")]
        public int Branch { get; set; }
        [Column("latestcommitted")]
        public int Latestcommitted { get; set; }
        [Column("latestversion")]
        public int Latestversion { get; set; }
    }
}
