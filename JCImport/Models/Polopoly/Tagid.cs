﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

#nullable disable

namespace JCImport.Models.Polopoly
{
    [Keyless]
    [Table("tagid")]
    [Index(nameof(Id), Name = "tagidIdx", IsUnique = true)]
    [Index(nameof(Name), Name = "tagidNameIdx", IsUnique = true)]
    public partial class Tagid
    {
        [Column("id")]
        public int Id { get; set; }
        [Required]
        [Column("name")]
        [StringLength(128)]
        public string Name { get; set; }
    }
}
