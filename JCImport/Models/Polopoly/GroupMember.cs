﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

#nullable disable

namespace JCImport.Models.Polopoly
{
    [Keyless]
    [Table("groupMember")]
    [Index(nameof(GroupId), Name = "groupMembersIdIdx")]
    [Index(nameof(PrincipalId), Name = "groupMembersMemIdx")]
    public partial class GroupMember
    {
        [Column("groupId")]
        public int GroupId { get; set; }
        [Required]
        [Column("principalId")]
        [StringLength(32)]
        public string PrincipalId { get; set; }
    }
}
