﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

#nullable disable

namespace JCImport.Models.Polopoly
{
    [Keyless]
    [Table("contentlocks")]
    [Index(nameof(Userid), nameof(Locktimeouttime), Name = "clLockedByIdx")]
    [Index(nameof(Major), nameof(Minor), Name = "contentlockIdx", IsUnique = true)]
    public partial class Contentlock
    {
        [Column("major")]
        public int Major { get; set; }
        [Column("minor")]
        public int Minor { get; set; }
        [Column("version")]
        public int Version { get; set; }
        [Required]
        [Column("userid")]
        [StringLength(32)]
        public string Userid { get; set; }
        [Column("sessionkey")]
        [StringLength(128)]
        public string Sessionkey { get; set; }
        [Column("locktimeouttime")]
        public int Locktimeouttime { get; set; }
    }
}
