﻿using Contentful.Core.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace JCImport.Models
{
    public class Image : ContentfulBase
    {
        public static Image New(string id)
        {
            return new Image
            {
                Sys = new SystemProperties { Id = id }
            };
        }
        public int Id { get; set; }
        [StringLength(255)]
        public string Title { get; set; }
        [Image]
        public object ImageAsset { get; set; }
        [StringLength(255)]
        public string Url { get; set; }
        [StringLength(255)]
        public string CitationText { get; set; }
        [StringLength(255)]
        public string CitationUrl { get; set; }
        public string Description { get; set; }
        public int Width { get; set; }
        public int Height { get; set; }
    }
}
