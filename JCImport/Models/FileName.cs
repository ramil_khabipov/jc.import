﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace JCImport.Models
{
    public class FileName
    {
        public int Height { get; set; }
        public string FilePath { get; set; }
        public int Width { get; set; }
    }
}
