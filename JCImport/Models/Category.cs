﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace JCImport.Models
{
    public class Category : ContentfulBase
    {
        public int Id { get; set; }
        [StringLength(255)]
        public string Title { get; set; }
        public Category ParentCategory { get; set; }
        public ICollection<Category> ChildCategories { get; set; }
    }
}
