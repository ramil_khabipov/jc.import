﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace JCImport.Models
{
    public class FileInfo
    {
        public string Title { get; set; }
        public string CitationText { get; set; }
        public string CitationUrl { get; set; }
        public string Description { get; set; }
    }
}
