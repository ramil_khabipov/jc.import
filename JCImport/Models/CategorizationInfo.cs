﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace JCImport.Models
{
    public class CategorizationInfo
    {
        public List<Dimension> Dimensions { get; set; }
    }

    public class Dimension
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public bool Enumerable { get; set; }
        public List<Entity> Entities { get; set; }
    }

    public class Entity
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public List<Entity> Entities { get; set; }
    }
}
