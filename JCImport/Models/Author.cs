﻿using Contentful.Core.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace JCImport.Models
{
    public class Author : ContentfulBase
    {
        public static Author New(string id)
        {
            return new Author
            {
                Sys = new SystemProperties { Id = id }
            };
        }
        public int Id { get; set; }
        [StringLength(255)]
        public string Title { get; set; }
        public ICollection<Image> Images { get; set; }
        public string Biography { get; set; }
        [StringLength(255)]
        public string Email { get; set; }
        public ICollection<Article> Articles { get; set; }
    }
}
