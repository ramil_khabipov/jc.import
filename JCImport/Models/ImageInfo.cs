﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace JCImport.Models
{
    public class ImageInfo
    {
        public string FileUri { get; set; }
        public int ImageWidth { get; set; }
        public int ImageHeight { get; set; }
    }
}
