﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace JCImport.Models
{
    public class Entries : ContentfulBase
    {
        public IEnumerable<object> Items { get; set; }
    }
}
