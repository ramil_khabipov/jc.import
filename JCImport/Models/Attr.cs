﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace JCImport.Models
{
    public enum Attr
    {
        Author = 1578,
        Body = 4191,
        Title = 22,
        Headline = 4461,
        SubTitle = 4596,
        ShortTitle = 7511,
        Biography = 3749,
        Rating = 4744,
        Priority = 4201,
        FactsTitle = 7507,
        FactsBody = 7510,
        EditedDate = 4143,
        PublishedDate = 7506,
        EditedDateN = 4149,
        PublishedDateN = 4597,
        HasImages = 3751,
        HasRelated = 4145,
        HasResources = 3483,
        IsSponsored = 10664,
        IsPromoted = 7397,
        TitleImage = 4246,
        ImageInfoJson = 3800,
        FileNameJson = 3801,
        FileInfoJson = 1096,
        ReviewInfo = 4743,
        Categorization = 4535,
        Email = 3753,
        OnlineState = 4147,
        ShowContent = 4148,
    }
}
