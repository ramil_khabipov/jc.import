﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace JCImport.Pages
{
    public class IndexModel : PageModel
    {
        private readonly ILogger<IndexModel> _logger;
        private readonly IImporter importer;

        public string ImportFinished { get; set; }

        public IndexModel(ILogger<IndexModel> logger, IImporter importer)
        {
            _logger = logger;
            this.importer = importer;
        }

        public void OnGet()
        {

        }

        public async Task<IActionResult> OnGetImportAsync()
        {
            var tablesProcessed = await importer.RunAsync();

            ImportFinished = $"Import finished, {tablesProcessed} tables processed";

            return new JsonResult(ImportFinished);
        }
    }
}
