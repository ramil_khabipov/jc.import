﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace JCImport.Extensions
{
    public static class StringExtensions
    {
        public static string ToText(this string input)
        {
            return input == null ? null : input.Replace("#000a", " ")
                .Replace("#0027", "'")
                .Replace("#2013", "–")
                .Replace("#2014", "—")
                .Replace("#2018", "‘")
                .Replace("#2019", "’")
                .Replace("#201a", "‚")
                .Replace("#201c", "“")
                .Replace("#201d", "”")
                .Replace("#201e", "„")
                .Replace("#2026", "…")
                .Replace("#2027", " ")
                .Replace("#2028", " ")
                .Replace("#2029", " ");
        }
    }
}
